const bcrypt = require('bcryptjs')
const jwt = require('jsonwebtoken')
const User = require('../model/user')


const login = async (req, res ) => {
    try{
        //Get user input
        const {email,password} = req.body
        //validate user input
        if(!(email && password)){
            console.log(`---- Enter email: ${email} and password: ${password} is requied ----`)
            res.status(400).send("Enter email and password is requied")
        }
          //check if user in database
          const user = await User.findOne({email})
        if(user && (await bcrypt.compare(password, user.password))){
            //generate token
            const token = jwt.sign({
                user_id: user._id,
                email
            },
            process.env.TOKEN_KEY,
            {
                expiresIn: process.env.EXPIRED
            })
            user.token = token;

            console.log(`--- Login Sucessfully! ---`)
            res.status(200).json(user)
        }
        console.log(`--- Invalid token ---`)
        res.status(400).send("Invalid token")

    }catch(err){
        console.log(err)
    }
}

module.exports = login
