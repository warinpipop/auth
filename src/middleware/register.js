
const bcrypt = require('bcryptjs')
const jwt = require('jsonwebtoken')
const User = require('../model/user')
const register = async ( req,res ) => {
try{
    const { first_name, last_name, email, password } = req.body

    //validate user input 
    if(!(email && first_name && last_name && password)) {
        console.log("--- All input is requried ---")
        res.status(400).send("All input is requried")
    }

    //check if user already exist
    const oldUser = await User.findOne({email})

    if(oldUser) {
        console.log("--- User already exist ---")
        return res.status(409).send("User already exist.")
    }

    //Encrypt user password
    encryptedPassword = await bcrypt.hash(password, 10)

    //Create user in database
    const user = await User.create({
        first_name,
        last_name,
        email: email.toLowerCase(),
        password: encryptedPassword
    })

    //generate token
    const token = jwt.sign({
        user_id: user._id,
        email
    },
    process.env.TOKEN_KEY,
    {
        expiresIn: process.env.EXPIRED
    })
    //save token
    user.token = token

    //send status
    console.log(`Create ${user}`)
    res.status(201).json(user)

}catch(err) {
    console.log(err)
}
}
module.exports = register