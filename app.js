require('dotenv').config();
require('./configs/mongo').connect()
const bodyParser = require('body-parser')

const express = require('express')

const app = express()
const User = require('./src/model/user')

//middleware
const auth = require('./src/middleware/auth')
const register = require('./src/middleware/register')
const login = require('./src/middleware/login')

app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())
//Register
app.post("/register", register)
//login
app.get("/login", login )

app.get('/user', auth, async(req,res)=> {
    const {email} = req.body
    const user = await User.findOne({email})
    res.status(200).json(user)
})


module.exports = app;